import { isNgTemplate } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'send the data to the child';
  data = "x";
  updateData(item: string)
  {
    alert(item);
    this.data = item;
   }
}
