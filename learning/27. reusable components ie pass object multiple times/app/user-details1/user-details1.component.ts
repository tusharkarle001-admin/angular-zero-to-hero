import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-details1',
  templateUrl: './user-details1.component.html',
  styleUrls: ['./user-details1.component.css']
})
export class UserDetails1Component implements OnInit {

  constructor() { }
  @Input() item: any;
  ngOnInit(): void {
  }

}
