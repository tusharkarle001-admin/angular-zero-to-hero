import { isNgTemplate } from '@angular/compiler';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Angular Basic Pipes';
  loginForm = new FormGroup({
    user: new FormControl('',[Validators.required]),
    password: new FormControl('')
  });

  loginUser() {
    console.log(this.loginForm.value);
  }
  get user() {
    return this.loginForm.get('user');
  }
}
