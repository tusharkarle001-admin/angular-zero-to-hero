import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'hello tushar';
  users = [
    {
      name: 'sam',
      phone: 122,
      socialAccount: ['facebook', 'insta', 'whatsapp'],
    },
    {
      name: 'peter',
      phone: 123,
      socialAccount: ['facebook', 'insta', 'whatsapp'],
    },
    {
      name: 'pal',
      phone: 124,
      socialAccount: ['facebook', 'insta', 'whatsapp'],
    },
  ];
}
