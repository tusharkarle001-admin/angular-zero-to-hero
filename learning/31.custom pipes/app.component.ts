import { isNgTemplate } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Angular Basic Pipes';
  /* pipes are nothing but using it property in html uppercase ,lowercase , showing only date  */
  today = Date();
  user = {
    name: "tushar",
    gmail:"karle@gmail.com"
  }
}
