import { Component } from '@angular/core';
import { reduce } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'hello tushar';
  color = "red";
  bgColor = "green";
  updateColor() {
    this.color = "blue";
    this.bgColor = "red";
  }
}
