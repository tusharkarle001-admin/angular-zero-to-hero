import { isNgTemplate } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Two way binding';
  setValue(data: string) {
    console.log(data);
  }

  // how to define and initialize var
  data: number = 12;

  getdata(item: number) {
    return item;
  }
  getdata1(item: number | boolean) {
    if (typeof item == 'number') {
      return item;
    }
    return item;
  }
  getdata2(item: { name: string; id: number }) {
    return item;
  }

  // declare the var as object
  item: { name: string; id: number } = { name: 'tushar', id: 12 };

  // declare an array
  itemArray: string[] = ['tushar', 'karle'];
}
