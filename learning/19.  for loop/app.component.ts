import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'hello tushar';
  users = ['tushar', 'rushi', 'vaibhav', 'sanket'];
  usersDetails = [
    {
      name: 'tushar',
      email: 'tusharkarle001@gmail.com',
      phone: 1,
    },
    {
      name: 'rushi',
      email: 'rushi@gmail.com',
      phone: 2,
    },
    {
      name: 'vaibhav',
      email: 'vaibhav@gmail.com',
      phone: 3,
    },
  ];
}
