import { isNgTemplate } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = ' To do list';
  data = 20;
  updateData() {
    this.data = Math.floor(Math.random()*10);
  }
}
