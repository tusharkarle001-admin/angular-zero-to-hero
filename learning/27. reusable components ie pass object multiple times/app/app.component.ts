import { isNgTemplate } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = ' To do list';
  userDetails = [
    {
      name: 'tushar karle',
      email: 'tusharkarle001@gmail.com',
    },
    {
      name: 'Bhaskar varma',
      email: 'bhaskarvarma@gmail.com',
    },
    {
      name: 'rushi',
      email: 'rushi@gmail.com',
    },
    {
      name: 'tushar karle',
      email: 'tusharkarle001@gmail.com',
    },
  ];
}
